/*
 *  ai.c v2.0
 *  Mr Pass.  Brain the size of a planet!
 *
 *  Created by Richard Buckland on 14/05/11.
 *  Copyright 2011 Licensed under Creative Commons SA-BY-NC 3.0. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Game.h"
#include "ai.h"

#define NUM_STUDENT_TYPES  6

typedef struct _students {
    int values[NUM_UNIS+1][NUM_STUDENT_TYPES];
} Students;

Students getStudentCounts(Game game) {
    printf("getting students counts:\n");
    printf("UNI    THD    BPS    BQN     MJ    MTV   MMON\n");
    Students students;
    for (int uni = UNI_A; uni <= UNI_C; uni++) {
        printf("  %d", uni);
        for (int discipline = 0; discipline < NUM_STUDENT_TYPES; discipline++) {
            students.values[uni][discipline] = getStudents(game, uni, discipline);
            printf("     %2d", students.values[uni][discipline]);
        }
        printf("\n");
    }
    printf("\n");
    return students;
}

// Given a uni, htis function returns a string giving its name
static char* uniString(int uni) {
    char* result;
    if (uni == NO_ONE) {
        result = "NO_ONE";
    } else if (uni == UNI_A) {
        result = "UNI_A";
    } else if (uni == UNI_B) {
        result = "UNI_B";
    } else if (uni == UNI_C) {
        result = "UNI_C";
    } else {
        printf("Error: Game.c: uniString: invalid uni\n");
        result = "INVALID_UNI";
    }
    return result;
}

/*
// Given a campuscode, htis function returns a string giving its name
static char* campusString(int campus) {
    char* result;
    if (campus == VACANT_VERTEX) {
        result = "VACANT_VERTEX";
    } else if (campus == CAMPUS_A) {
        result = "CAMPUS_A";
    } else if (campus == CAMPUS_B) {
        result = "CAMPUS_B";
    } else if (campus == CAMPUS_C) {
        result = "CAMPUS_C";
    } else if (campus == GO8_A) {
        result = "GO8_A";
    } else if (campus == GO8_B) {
        result = "GO8_B";
    } else if (campus == GO8_C) {
        result = "GO8_C";
    } else {
        printf("Error: Game.c: campusString: invalid campusCode\n");
        result = "INVALID_CAMPUS_CODE";
    }
    return result;
}

// Given am arc code, this function returns a string giving its name
static char* arcString(int campus) {
    char* result;
    if (campus == VACANT_ARC) {
        result = "VACANT_ARC";
    } else if (campus == ARC_A) {
        result = "ARC_A";
    } else if (campus == ARC_B) {
        result = "ARC_B";
    } else if (campus == ARC_C) {
        result = "ARC_C";
    } else {
        printf("Error: Game.c: arcString: invalid arc code\n");
        result = "INVALID_ARC_CODE";
    }
    return result;
}*/

// Given a uni, htis function returns a string giving its name
static char* disciplineString(int discipline) {
    char* result;
    if (discipline == STUDENT_THD) {
        result = "THD";
    } else if (discipline == STUDENT_BPS) {
        result = "BPS";
    } else if (discipline == STUDENT_BQN) {
        result = "BQN";
    } else if (discipline == STUDENT_MJ) {
        result = "MJ";
    } else if (discipline == STUDENT_MTV) {
        result = "MTV";
    } else if (discipline == STUDENT_MMONEY) {
        result = "MON";
    } else {
        printf("Error: Game.c: disciplineString: invalid discipline\n");
        result = "Invalid discipline";
    }
    return result;
}

// Given an actionCode, this function returns a string giving its name
static char* actionString(int actionCode) {
    char* result;
    if (actionCode == PASS) {
        result = "PASS";
    } else if (actionCode == BUILD_CAMPUS) {
        result = "BUILD_CAMPUS";
    } else if (actionCode == BUILD_GO8) {
        result = "BUILD_GO8";
    } else if (actionCode == OBTAIN_ARC) {
        result = "OBTAIN_ARC";
    } else if (actionCode == START_SPINOFF) {
        result = "START_SPINOFF";
    } else if (actionCode == OBTAIN_PUBLICATION) {
        result = "OBTAIN_PUBLICATION";
    } else if (actionCode == OBTAIN_IP_PATENT) {
        result = "OBTAIN_IP_PATENT";
    } else if (actionCode == RETRAIN_STUDENTS) {
        result = "RETRAIN_STUDENTS";
    } else {
        result = "INVALID_ACTION";
        printf("Error: Game.c: actionString: invalid action: %d\n", actionCode);
    }
    return result;
}

static void printBoard(Game game) {
    printf("Printing board:\n");
    printf("RegionID diceValue discipline\n");
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        int dice = getDiceValue(game, regionID);
        int discipline = getDiscipline(game, regionID);
        printf("      %2d        %2d", regionID, dice);
        printf("   %s\n", disciplineString(discipline));
    }
    printf("\n");
}


/*static void printStudentCounts(Player players[]) {
    printf("\n  UNI    THD    BPS    BQN     MJ    MTV   MMON\n");
    for (int uni = UNI_A; uni <= UNI_C; uni++) {
        printf("%s", uniString(uni));
        for (int discipline = 0; discipline < NUM_STUDENT_TYPES; discipline++) {
            printf("     %2d", players[uni].students[discipline]);
        }
        printf("\n");
    }
    printf("\n");
}*/


// Find how many students in the given discipline are required to perform
// the selected action
int studentsUsedForActionInDiscipline(int actionCode, int discipline) {
    int result = 1000;
    if (actionCode == START_SPINOFF) {
        if (STUDENT_MJ <= discipline && discipline <= STUDENT_MMONEY) {
            result = 1;
        } else {
            result = 0;
        }
    }
    return result;
}


// Find how many extra students are required in the given discipline for
// the given uni to perform the given action
int studentsRequiredInDiscipline(Game game, int uni, int actionCode, int discipline) {
    int result = 0;
    // Get the number of students currently held
    int studentsHeld = getStudents(game, uni, discipline);
    printf("%ss currently held: %d. ", disciplineString(discipline), studentsHeld);
    // Get the number of extra students we need
    int studentsNeeded = studentsUsedForActionInDiscipline(actionCode, discipline);
    printf("Needed: %d. ", studentsNeeded);
    // If more are needed
    if (studentsHeld < studentsNeeded) {
        // add to the total how many more we need
        result = studentsNeeded - studentsHeld;
    }
    printf("%d additional %ss are required ", result, disciplineString(discipline));
    printf("for %s to %s\n", uniString(uni), actionString(actionCode));
    return result;
}


// How many extra students are necessary in the disciplines relevant
// to an action in order to make that action legal?
int additionalStudentsRequired(Game game, int uni, int actionCode) {
    int result = 0;
    for (int discipline = STUDENT_BPS; discipline < NUM_STUDENT_TYPES; discipline++) {
        // add to the total how many more we need
        result += studentsRequiredInDiscipline(game, uni, actionCode, discipline);
    }
    printf("In total, %d extra students are needed in the relevant ", result);
    printf("disciplines for %s to %s\n\n", uniString(uni), actionString(actionCode));
    return result;
}

// Find how many students can be created by retraining out of the students
// the uni has in the given discipline which are not required for the
// given action.
int studentsRetrainableInDiscipline(Game game, int uni, int actionCode, int discipline) {
    int result;
    // Get the number of students currently held
    int studentsHeld = getStudents(game, uni, discipline);
    
    // Get the number of students that must no be retrained
    int studentsToKeep = studentsUsedForActionInDiscipline(actionCode, discipline);
    
    // If there are students left to retrain,
    if (studentsHeld > studentsToKeep) {
        // Then this is how many can be retrained
        result =(studentsHeld - studentsToKeep) / 3;
    } else {
        result = 0; // :(
    }
    printf("%d %ss can be retrained ", result, disciplineString(discipline));
    printf("in order to %s\n", actionString(actionCode));
    return result;
}


// Find the total number of students that can be created in order to
// perfomr the given action, from the students it has that are not
// required for the actions
int studentsRetrainable(Game game, int uni, int actionCode) {
    int studentsTrainable = 0;

    for (int discipline = STUDENT_BPS; discipline < NUM_STUDENT_TYPES; discipline++) {
        studentsTrainable += studentsRetrainableInDiscipline(game, uni, actionCode, discipline);
    }
    printf("In total, %d students can be retrained", studentsTrainable);
    printf(" in order to %s\n\n", actionString(actionCode));
    return studentsTrainable;
}


// Determine whether the given uni has enough students to perform the
// given action after retraining students
int studentsCanBeTrainedForAction(Game game, int uni, int actionCode) {
    int result;
    int required = additionalStudentsRequired(game, uni, actionCode);
    int retrainable = studentsRetrainable(game, uni, actionCode);
    printf("\nit ");
    if (retrainable >= required) {
        printf("is ");
        result = TRUE;
    } else {
        printf("is not ");
        result = FALSE;
    }
    printf("possible to retrain students to %s\n", actionString(actionCode));
    return result;
}

// return an action to retrain students with the ultimate aim of having
// enough students to perform another action
action retrainForAction(Game game, int uni, int actionCode) {
    // For all student types, find how many can be created by retraining
    for (int from = 0; from < NUM_STUDENT_TYPES; from++) {
        int studentsTrainable = studentsRetrainableInDiscipline(game, uni, actionCode, from);
        // If any can be retrained from this discipline
        if (studentsTrainable > 0) {
            // Find a discipline to retrain to
            for (int to = 0; to < NUM_STUDENT_TYPES; to++) {
                int studentsNeeded = studentsRequiredInDiscipline(game, uni, actionCode, to);
                // if we find one which needs students, retrain them.
                if (studentsNeeded > 0) {
                    action retrain;
                    retrain.actionCode = RETRAIN_STUDENTS;
                    retrain.disciplineFrom = from;
                    retrain.disciplineTo = to;
                    printf("Retraining %ss", disciplineString(from));
                    printf(" to a %s...\n", disciplineString(to));
                    return retrain;
                }
            }
        }
    }

    printf("Error: failed to retrain students. This shouldn't happen.\n");
    action pass;
    pass.actionCode = PASS;
    return pass;
}

// Just retrain students and start a spinoff wherever possible
action spinoffBuilder (Game game) {
    printf("\n\nspinoffBuilder is thinking...\n");
    int uni = getWhoseTurn(game);
    action startSpinoff;
    startSpinoff.actionCode = START_SPINOFF;
    if (isLegalAction(game, startSpinoff)) {
        printf("It is legal to start a spinoff. Starting one...\n\n");
        return startSpinoff;
    } else {
        printf("It is illegal to start a spinoff.\n\n");
        if (studentsCanBeTrainedForAction(game, uni, START_SPINOFF)) {
            return retrainForAction(game, uni, START_SPINOFF);
        } else {
            action pass;
            pass.actionCode = PASS;
            return pass;
        }
    }
}



action decideAction (Game g) {
    if (getTurnNumber(g) / 3 == 0) {
        printBoard(g);
    }
    return spinoffBuilder(g);
}


/*
// Given a outcome of rolling two dice, the probability of that outcome
// is returned
float probabilityOfThrow(int value) {
    int sampleSpace = 36; // 6 possibilities for each die
    int eventSpace = 0;
    for (int die1 = 1; die1 <= 6; die1++) {
        for (int die2 = 1; die2 <= 6; die2++) {
            if (die1 + die2 == value) {
                eventSpace++;
            }
        }
    }
    return eventSpace/sampleSpace;
}


int readyToBuildArc(Game g, int player) {
    if (getStudents(g, player, STUDENT_BPS) >= 1 &&
        getStudents(g, player, STUDENT_BQN) >= 1) {
        return TRUE;
    } else {
        return FALSE;
    }
}


// given a game, attempt to retrain students so that there are enough to
// build an arc. Returns true  if successful:
action retrainForArc (Game g, int player) {
    action result;
    if (readyToBuildArc(g, player) == TRUE) {
        result.actionCode = PASS;
        return result;
    } else {
        int students [NUM_STUDENT_TYPES];
        for (int discipline = STUDENT_THD; discipline <= STUDENT_MMONEY; discipline++) {
            students[discipline] = getStudents(g, getWhoseTurn(g), discipline);
        }

        int requiredStudents; // Number extra BPS/BQNs needed
        requiredStudents = (1-students[STUDENT_BQN]) + (1-students[STUDENT_BPS]);

        int totalAvailableStudents; // the total number of BPS/BQN that
        // could be created from retraining
        for (int discipline = STUDENT_MJ; discipline <= STUDENT_MMONEY; discipline++) {
            totalAvailableStudents += students[discipline]/3;
        }

        if (totalAvailableStudents >= requiredStudents) {
            if (students[STUDENT_BPS] < 1) {
                // We need a BPS
                for (int discipline = STUDENT_MJ; discipline <= STUDENT_MMONEY; discipline++) {
                    if (students[discipline]/3 >= 1) {
                        result.actionCode = RETRAIN_STUDENTS;
                        result.disciplineFrom = discipline;
                        result.disciplineTo = STUDENT_BPS;
                        return result;
                    }
                }
            } else if (students[STUDENT_BQN] < 1) {
                // We need a BPS
                for (int discipline = STUDENT_MJ; discipline <= STUDENT_MMONEY; discipline++) {
                    if (students[discipline]/3 >= 1) {
                        result.actionCode = RETRAIN_STUDENTS;
                        result.disciplineFrom = discipline;
                        result.disciplineTo = STUDENT_BQN;
                        return result;
                    }
                }
            }
        } else {
            result.actionCode = PASS;
            return result;
        }
    }
}
    


// Given a discipline, this function returns how much we want students
// from taht discipline (0..1)
float valueOfDiscipline {
   return 1;
}*/