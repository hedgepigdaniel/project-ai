/*
 * Game.c
 * Daniel Playfair Cal, Radin Ahmed
 * 13/05/2012
 */

#include "Game.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

// Different types of objects on the board
#define OBJECT_NONE    0
#define OBJECT_VERTEX  1
#define OBJECT_EDGE     2
#define OBJECT_REGION  3

//define VACANT_VERTEX 0
#define NORMAL_CAMPUS  1
#define GO8_CAMPUS     2

#define NUM_REGIONS       19
#define NUM_STUDENT_TYPES  6
#define NUM_VERTICES      54
#define NUM_EDGES          72
#define PATH_LIMIT       150

// When the game hasn't started yet, it is this strange person's turn
#define TERRA_NULLIUS -1

#define TRUE  1
#define FALSE 0

// The number of rown and columns in the board struct. Not alalogous to
// the number of regions - it just has to be big enough to store all the
// objects in the correct layout
#define BOARD_COLUMNS  23
#define BOARD_ROWS     21

// Directions - used for the steps in a path and to describe the vectors
// between neighbouring objects in the board struct. Numbered from the
// right and then anticlockwise according to mathematical convention. Not
// all of these directions are valid in all cases.
#define DIR_RIGHT         0
#define DIR_RIGHT_ABOVE   1
#define DIR_ABOVE         2
#define DIR_LEFT_ABOVE    3
#define DIR_LEFT          4
#define DIR_LEFT_BELOW    5
#define DIR_BELOW         6
#define DIR_RIGHT_BELOW   7

#define NUM_DIRECTIONS    8

// Used when alternative directions (in angles) are used
#define DEGREES_IN_CIRCLE 360

// The starting grid coordinates and direction (in degrees) for a path
#define PATH_START_LOCATION       {10,20}
#define PATH_INITIAL_DIRECTION    300

// Path turning directions: number of degrees to turn (anticlockwise)
// to face in the right direction for the next step.
#define PATH_TURN_LEFT   60
#define PATH_TURN_RIGHT 300  /* 300 degrees = -60 degrees (mod 360) */
#define PATH_TURN_BACK  180

// Paths of the initial campuses and arcs for each player.
// These paths make the last hop through the initial arc to the campus.
#define INITIAL_PATH_A1 "RB"
#define INITIAL_PATH_A2 "RLLRRLRLRLR"
#define INITIAL_PATH_B1 "RLRRLRR"
#define INITIAL_PATH_B2 "RLRLLRLRLR"
#define INITIAL_PATH_C1 "LRLRL"
#define INITIAL_PATH_C2 "RLRLRLRRLR"

// KPI points that should be awarded
#define KPI_PER_ARC                     2
#define KPI_PER_CAMPUS                 10
#define KPI_PER_GO8                    20
#define KPI_PER_PATENT                 10
#define POINTS_FOR_MOST_ARCS           10
#define POINTS_FOR_MOST_PUBLICATIONS   10

// Types of retraining centre
//#define STUDENT_THD        0
//...
//#define STUDENT_MMONEY     5
#define RETRAINING_FLEXIBLE  6
#define RETRAINING_NONE      7

//Maximum number of G08s allowed on the game
#define MAX_GO8S 8

// When this is defined, the details of any interface function calls are
// printed to stdout
//#define PRINT_INTERFACE_CALLS    TRUE

// Player struct: holds data about a player
typedef struct _player {
    int arcs;       // The number of arc grants the player has
    int normalCampuses;  // The number of normal campuses the player has
    int GO8s;            // The number of GO8 campuses the player has
    int publications;    // The number of publications the player has
    int patents;         // The number of IP patents the player has
    // The number of students of each discipline the player has:
    int students [NUM_STUDENT_TYPES];
} Player;

// 2D Vector: used to represent coordinates on the grid struct as well as
// vectors used to find locations relative to other objects
typedef struct _vector {
    int x;
    int y;
} Vector;

// Region struct: holds information about a particular region
typedef struct _region {
    int diceValue;    // The dice value which will produce students
    int discipline;   // The type of students that will be produced
    Vector location;  // The coordinates of the region in the grid struct
} Region;

// Vertex struct: holds infomation about a particular vertex
typedef struct _vertex {
    int campus;            // The type of campus on the vertex
    int retrainingCentre;  // The type of retraining centre on the vertex
    Vector location; // The coordinates of the vertex in the grid struct
} Vertex;

// Edge struct: contains data particular edge
typedef struct _edge {
    int arc;          // Which player has an arc grant on the edge
    Vector location;  // The coordinates of the arc in the grid struct
} Edge;

// Cell: a cell in the board struct. Contains a reference to the object
// at its coordinates. This might be no object, or ONE of the below.
typedef struct _cell {
    int objectType;     // The type of object at this location
    Vertex* vertex;     // Pointer to the vertex at this location
    Region* region;     // Pointer to the region at this location
    Edge* edge;         // Pointer to the arc at this location
} Cell;

// Board struct: a two dimensional grid of Cells representing the game
// board. The cells should be populated to represent the board.
// The bottom left corner is {0,0}.
typedef Cell Board [BOARD_COLUMNS] [BOARD_ROWS];

// Game struct: stores all the persistent data about a game, including
// the state of the board and of all players
typedef struct _game {
    Board board;      // A grid representing the layout of the board
    Region regions [NUM_REGIONS];    // Data about each region
    Vertex vertices [NUM_VERTICES];  // Data about each vertex
    Edge edges [NUM_EDGES];             // Data about each arc
    Player players [NUM_UNIS + 1];   // Data about each player
    // ^^ we need an extra index for the NO_ONE (UNI_A starts at 1)
    int activePlayer;        // The player who's turn it is (-1 initially)
    int turnNumber;          // Rounds elapsed (-1 initially)
    int totalGO8s;           // Total number of GO8 campuses on the board
    int mostArcs;            // The player with the most arc grants
    int mostPublications;    // The player with the most publications
} GameStruct;

#ifdef PRINT_INTERFACE_CALLS

// Given a uni, htis function returns a string giving its name
static char* uniString(int uni) {
    char* result;
    if (uni == NO_ONE) {
        result = "NO_ONE";
    } else if (uni == UNI_A) {
        result = "UNI_A";
    } else if (uni == UNI_B) {
        result = "UNI_B";
    } else if (uni == UNI_C) {
        result = "UNI_C";
    } else {
        printf("Error: Game.c: uniString: invalid uni\n");
        result = "INVALID_UNI";
    }
    return result;
}

// Given a campuscode, htis function returns a string giving its name
static char* campusString(int campus) {
    char* result;
    if (campus == VACANT_VERTEX) {
        result = "VACANT_VERTEX";
    } else if (campus == CAMPUS_A) {
        result = "CAMPUS_A";
    } else if (campus == CAMPUS_B) {
        result = "CAMPUS_B";
    } else if (campus == CAMPUS_C) {
        result = "CAMPUS_C";
    } else if (campus == GO8_A) {
        result = "GO8_A";
    } else if (campus == GO8_B) {
        result = "GO8_B";
    } else if (campus == GO8_C) {
        result = "GO8_C";
    } else {
        printf("Error: Game.c: campusString: invalid campusCode\n");
        result = "INVALID_CAMPUS_CODE";
    }
    return result;
}

// Given am arc code, this function returns a string giving its name
static char* arcString(int campus) {
    char* result;
    if (campus == VACANT_ARC) {
        result = "VACANT_ARC";
    } else if (campus == ARC_A) {
        result = "ARC_A";
    } else if (campus == ARC_B) {
        result = "ARC_B";
    } else if (campus == ARC_C) {
        result = "ARC_C";
    } else {
        printf("Error: Game.c: arcString: invalid arc code\n");
        result = "INVALID_ARC_CODE";
    }
    return result;
}

// Given a uni, htis function returns a string giving its name
static char* disciplineString(int discipline) {
    char* result;
    if (discipline == STUDENT_THD) {
        result = "THD";
    } else if (discipline == STUDENT_BPS) {
        result = "BPS";
    } else if (discipline == STUDENT_BQN) {
        result = "BQN";
    } else if (discipline == STUDENT_MJ) {
        result = "MJ";
    } else if (discipline == STUDENT_MTV) {
        result = "MTV";
    } else if (discipline == STUDENT_MMONEY) {
        result = "MON";
    } else {
        printf("Error: Game.c: disciplineString: invalid discipline\n");
        result = "Invalid discipline";
    }
    return result;
}

// Given an actionCode, this function returns a string giving its name
static char* actionString(int actionCode) {
    char* result;
    if (actionCode == PASS) {
        result = "PASS";
    } else if (actionCode == BUILD_CAMPUS) {
        result = "BUILD_CAMPUS";
    } else if (actionCode == BUILD_GO8) {
        result = "BUILD_GO8";
    } else if (actionCode == OBTAIN_ARC) {
        result = "OBTAIN_ARC";
    } else if (actionCode == START_SPINOFF) {
        result = "START_SPINOFF";
    } else if (actionCode == OBTAIN_PUBLICATION) {
        result = "OBTAIN_PUBLICATION";
    } else if (actionCode == OBTAIN_IP_PATENT) {
        result = "OBTAIN_IP_PATENT";
    } else if (actionCode == RETRAIN_STUDENTS) {
        result = "RETRAIN_STUDENTS";
    } else {
        result = "INVALID_ACTION";
        printf("Error: Game.c: actionString: invalid action: %d\n", actionCode);
    }
    return result;
}


void printStudentCounts(Player players[]) {
    printf("\n  UNI    THD    BPS    BQN     MJ    MTV   MMON\n");
    for (int uni = UNI_A; uni <= UNI_C; uni++) {
        printf("%s", uniString(uni));
        for (int discipline = 0; discipline < NUM_STUDENT_TYPES; discipline++) {
            printf("     %2d", players[uni].students[discipline]);
        }
        printf("\n");
    }
    printf("\n");
}


#endif // PRINT_INTERFACE_CALLS

// Given a campus code, return the uni that owns it
int campusOwner(int campusCode) {
    int result;
    if (campusCode == CAMPUS_A || campusCode == GO8_A) {
        result = UNI_A;
    } else if (campusCode == CAMPUS_B || campusCode == GO8_B) {
        result = UNI_B;
    } else if (campusCode == CAMPUS_C || campusCode == GO8_C) {
        result = UNI_C;
    } else if (campusCode == VACANT_VERTEX) {
        result = NO_ONE;
    } else {
        printf("error: Game.c: campusOwner: invalid campusCode: %d\n", campusCode);
        result = -1;
    }
    return result;
}

// Given a campus code, return what type of campus it is
int campusType(int campusCode) {
    int result;
    if (campusCode == CAMPUS_A ||
        campusCode == CAMPUS_B ||
        campusCode == CAMPUS_C) {
        result = NORMAL_CAMPUS;
    } else if (campusCode == GO8_A ||
               campusCode == GO8_B ||
               campusCode == GO8_C ) {
            result = GO8_CAMPUS;
    } else if (campusCode == VACANT_VERTEX) {
        result = VACANT_VERTEX;
    } else {
        printf("error: Game.c: campusType: invalid campusCode: %d\n", campusCode);
        result = -1;
    }
    return result;
}

// given a uni, return a code for a campus owned by that uni
int campusForUni(int uni) {
    int result;
    if (uni == UNI_A) {
        result = CAMPUS_A;
    } else if (uni == UNI_B) {
        result = CAMPUS_B;
    } else if (uni == UNI_C) {
        result = CAMPUS_C;
    } else if (uni == NO_ONE) {
        result = VACANT_VERTEX;
    } else {
        result = -1;
        printf("error: Game.c: campusForUni: invalud uni: %d\n", uni);
    }
    return result;
}


// given a uni, return a code for a GO8 owned by that uni
int GO8ForUni(int uni) {
    int result;
    if (uni == UNI_A) {
        result = GO8_A;
    } else if (uni == UNI_B) {
        result = GO8_B;
    } else if (uni == UNI_C) {
        result = GO8_C;
    } else if (uni == NO_ONE) {
        result = VACANT_VERTEX;
    } else {
        result = -1;
        printf("error: Game.c: GO8ForUni: invalud uni: %d\n", uni);
    }
    return result;
}

// given a uni, return a code for an edge with an arc owned by that uni
int arcForUni(int uni) {
    int result;
    if (uni == UNI_A) {
        result = ARC_A;
    } else if (uni == UNI_B) {
        result = ARC_B;
    } else if (uni == UNI_C) {
        result = ARC_C;
    } else if (uni == NO_ONE) {
        result = VACANT_ARC;
    } else {
        result = -1;
        printf("error: Game.c: arcForUni: invalud uni: %d\n", uni);
    }
    return result;
}

// given an arc code, return who owns the arc
int arcOwner(int arcCode) {
    int result;
    if (arcCode == ARC_A) {
        result = UNI_A;
    } else if (arcCode == ARC_B) {
        result = UNI_B;
    } else if (arcCode == ARC_C) {
        result = UNI_C;
    } else if (arcCode == VACANT_ARC) {
        result = NO_ONE;
    } else {
        result = -1;
        printf("error: Game.c: arcOwner: invalid arcCode: %d\n", arcCode);
    }
    return result;
}



// Given a regionID (0..18) this function returns the coordinate in the
// board struct where the region with regionID is.
Vector regionIdToVector(Game game, int regionID) {
    return game->regions[regionID].location;
}

// given two vectors, the sum of both is returned
Vector sum(Vector vector1, Vector vector2) {
    Vector result;
    result.x = vector1.x + vector2.x;
    result.y = vector1.y + vector2.y;
    return result;
}

// given a vector and a scalar, the vector is scaled by the scalar
Vector scaleVector(Vector vector, int scalar) {
    vector.x *= scalar;;
    vector.y *= scalar;
    return vector;
}


// Given a vector, check whether it corresponds with a location on the
// board struct. (Ie it is not outside the bounds of the struct)
int isOnBoard(Vector location) {
    if ( (0 <= location.x && location.x < BOARD_COLUMNS) &&
        (0 <= location.y && location.y < BOARD_ROWS) ) {
        return TRUE;
    } else {
        return FALSE;
    }
}

// Given a direction, this function returns the vector from an edge to the
// adjacent edge in the given direction. If the given direction is
// invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
Vector vectorBetweenEdges(int direction) {
    Vector result;
    if (direction == 30 || direction == DIR_RIGHT_ABOVE) {
        result.x = 2;
        result.y = 1;
    } else if (direction == 90 || direction == DIR_ABOVE) {
        result.x = 0;
        result.y = 2;
    } else if (direction == 150 || direction == DIR_LEFT_ABOVE) {
        result.x = -2;
        result.y = 1;
    } else if (direction == 210 || direction == DIR_LEFT_BELOW) {
        result.x = -2;
        result.y = -1;
    } else if (direction == 270 || direction == DIR_BELOW) {
        result.x = 0;
        result.y = -2;
    } else if (direction == 330 || direction == DIR_RIGHT_BELOW) {
        result.x = 2;
        result.y = -1;
    } else {
        // The direction given was not valid
        result.x = 0;
        result.y = 0;
    }
    return result;
}

// Given a direction, this function returns the vector from an arc to the
// adjacent region in the given direction, and vice versa. If the given
// direction is invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
Vector vectorBetweenEdgeAndRegion(int direction) {
    return vectorBetweenEdges(direction);
}

// Given a direction, this function returns the vector from a region to
// the adjacent region in the given direction. If the given direction is
// invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
Vector vectorBetweenRegions(int direction) {
    return scaleVector(vectorBetweenEdges(direction), 2);
}

// Given a direction, this function returns the vector from a region to
// the adjacent vertex in the given direction, or vice versa. If the
// given direction is invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
Vector vectorBetweenRegionAndVertex(int direction) {
    Vector result;
    if (direction == 0 || direction == DIR_RIGHT) {
        result.x = 3;
        result.y = 0;
    } else if (direction == 60 || direction == DIR_RIGHT_ABOVE) {
        result.x = 1;
        result.y = 2;
    } else if (direction == 120 || direction == DIR_LEFT_ABOVE) {
        result.x = -1;
        result.y = 2;
    } else if (direction == 180 || direction == DIR_LEFT) {
        result.x = -3;
        result.y = 0;
    } else if (direction == 240 || direction == DIR_LEFT_BELOW) {
        result.x = -1;
        result.y = -2;
    } else if (direction == 300 || direction == DIR_RIGHT_BELOW) {
        result.x = 1;
        result.y = -2;
    } else {
        // The direction given was not valid
        result.x = 0;
        result.y = 0;
    }
    return result;
}

// Given a direction, this function returns the vector from an edge to
// the adjacent vertex in the given direction, or vice versa. If the
// given direction is invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
Vector vectorBetweenEdgeAndVertex(int direction) {
    Vector result;
    if (direction == 0 || direction == DIR_RIGHT) {
        result.x = 1;
        result.y = 0;
    } else if (direction == 60 || direction == DIR_RIGHT_ABOVE) {
        result.x = 1;
        result.y = 1;
    } else if (direction == 120 || direction == DIR_LEFT_ABOVE) {
        result.x = -1;
        result.y = 1;
    } else if (direction == 180 || direction == DIR_ABOVE) {
        result.x = -1;
        result.y = 0;
    } else if (direction == 240 || direction == DIR_LEFT_BELOW) {
        result.x = -1;
        result.y = -1;
    } else if (direction == 300 || direction == DIR_RIGHT_BELOW) {
        result.x = 1;
        result.y = -1;
    } else {
        // The direction given was not valid
        result.x = 0;
        result.y = 0;
    }
    return result;
}

// Given a direction, this function returns the vector from a vertex to
// the adjacent vertex in the given direction. If the given direction is
// invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
Vector vectorBetweenVertices(int direction) {
    return scaleVector(vectorBetweenEdgeAndVertex(direction), 2);
}

// Print an error when we get an invalid path.
void rejectPath(path badPath, char* reason) {
    printf("Error: Game.c: received an invalid path: \"%s\"\n", badPath);
    printf("reason: %s\n", reason);
}

// Given a path and whether an edge or vertex should be returned, this
// function returns the coordinates of the object at the end of the path
Vector locationFromPath(Board board, path pathToVertex, int objectType) {
    Vector location = PATH_START_LOCATION;
    int direction = PATH_INITIAL_DIRECTION;
    Vector defaultResult = {0,0}; // returned for invalid paths
    for (int pathIndex = 0; pathToVertex[pathIndex] != 0; pathIndex++) {
        if (pathToVertex[pathIndex] == 'L') {
            direction += PATH_TURN_LEFT;
        } else if (pathToVertex[pathIndex] == 'R') {
            direction += PATH_TURN_RIGHT;
        } else if (pathToVertex[pathIndex] == 'B') {
            direction += PATH_TURN_BACK;
        } else {
            rejectPath(pathToVertex, "Contains an invalid character!");
            return defaultResult;
        }
        direction %= DEGREES_IN_CIRCLE;
        //printf("Read path element: %c, direction: %d\n", pathToVertex[pathIndex], direction);
        location = sum(location, vectorBetweenVertices(direction));
        //printf("Location: %d, %d\n", location.x, location.y);

        // At each step, make sure we are still on the board
        if (isOnBoard(location)) {
            // the above only checks for the rectangle encompassing the board
            if (board[location.x][location.y].vertex == NULL) {
                rejectPath(pathToVertex, "goes off board");
                return defaultResult;
            }
        } else {
            rejectPath(pathToVertex, "goes off board");
            return defaultResult;
        }
        if (pathIndex >= PATH_LIMIT) {
            rejectPath(pathToVertex, "longer than 150 characters");
            return defaultResult;
        }
    }

    // Now return the location
    if (objectType == OBJECT_VERTEX) {
        return location;
    } else if (objectType == OBJECT_EDGE) {
        // Go backwards to the arc we just passed
        direction += 180;
        direction %= DEGREES_IN_CIRCLE;
        // Since the above ensures that the path is valid, there is no
        // need to check if the arc exists.
        return sum(location, vectorBetweenEdgeAndVertex(direction));
    } else {
        printf("Error: Game.c: locationFromPath: Invalid objectType\n");
        return defaultResult;
    }
}

// Given a path, this function returns a pointer to the vertex at its end
Vertex* vertexFromPath(path pathToVertex, Board board) {
    Vector location = locationFromPath(board, pathToVertex, OBJECT_VERTEX);
    return board[location.x][location.y].vertex;
}

// Given a path, this function returns a pointer to the edge at its end
Edge* edgeFromPath(path pathToEdge, Board board) {
    Vector location = locationFromPath(board, pathToEdge, OBJECT_EDGE);
    return board[location.x][location.y].edge;
}

// Given the location of an object, this function returns a pointer to
// the neighbouring arc in the specified direction
Edge* relativeEdge(Board board, Vector base, int direction) {
    Vector location; // This will hold the coordinates of the arc
    if (!isOnBoard(base)) {
        printf("Error: Game.c: relativeEdge given invalid location");
        return NULL;
    }
    // Depending on the base object type, add the appropriate vector.
    if (board[base.x][base.y].objectType == OBJECT_EDGE) {
        location = sum(base, vectorBetweenEdges(direction));
    } else if (board[base.x][base.y].objectType == OBJECT_REGION) {
        location = sum(base, vectorBetweenEdgeAndRegion(direction));
    } else if (board[base.x][base.y].objectType == OBJECT_VERTEX) {
        location = sum(base, vectorBetweenEdgeAndVertex(direction));
    } else {
        printf("Game.c: invalid base object passed to relativEdge\n");
        return NULL;
    }
    // Return the pointer to the required arc. This will be null if there
    // is no arc there
    if (isOnBoard(location)) {
        return board[location.x][location.y].edge;
    } else {
        return NULL;
    }
}

// Given the location of an object, this function returns a pointer to
// the neighbouring region in the specified direction
Region* relativeRegion(Board board, Vector base, int direction) {
    Vector location; // This will hold the coordinates of the arc
    if (!isOnBoard(base)) {
        printf("Error: Game.c: relativeRegion given invalid location");
        return NULL;
    }
    // Depending on the base object type, add the appropriate vector.
    if (board[base.x][base.y].objectType == OBJECT_EDGE) {
        location = sum(base, vectorBetweenEdgeAndRegion(direction));
    } else if (board[base.x][base.y].objectType == OBJECT_REGION) {
        location = sum(base, vectorBetweenRegions(direction));
    } else if (board[base.x][base.y].objectType == OBJECT_VERTEX) {
        location = sum(base, vectorBetweenRegionAndVertex(direction));
    } else {
        printf("Game.c: invalid base object passed to relativeRegion\n");
        return NULL;
    }
    // Return the pointer to the required region. This will be null if
    // there is no region there
    if (isOnBoard(location)) {
        return board[location.x][location.y].region;
    } else {
        return NULL;
    }
}

// Given the location of an object, this function returns a pointer to
// the neighbouring vertex in the specified direction
Vertex* relativeVertex(Board board, Vector base, int direction) {
    Vector location; // This will hold the coordinates of the arc
    if (!isOnBoard(base)) {
        printf("Error: Game.c: relativeVertex given invalid location");
        return NULL;
    }
    // Depending on the base object type, add the appropriate vector.
    if (board[base.x][base.y].objectType == OBJECT_EDGE) {
        location = sum(base, vectorBetweenEdgeAndVertex(direction));
    } else if (board[base.x][base.y].objectType == OBJECT_REGION) {
        location = sum(base, vectorBetweenRegionAndVertex(direction));
    } else if (board[base.x][base.y].objectType == OBJECT_VERTEX) {
        location = sum(base, vectorBetweenVertices(direction));
    } else {
        printf("Game.c: invalid base object passed to relativeRegion\n");
        return NULL;
    }
    // Return the pointer to the required vertex. This will be null if
    // there is no vertex there
    if (isOnBoard(location)) {
        return board[location.x][location.y].vertex;
    } else {
        return NULL;
    }
}


// Given a board, regions, and arrays of the disciplines and dice values
// of each region, the regions will be initialised
void initRegions(Board board,
                 Region regions[NUM_REGIONS],
                 int discipline[NUM_REGIONS],
                 int dice[]) {
    // First note the locations of all the regions (in order by regionID)
    Vector locations[NUM_REGIONS] = {
                  {19, 6}, {19,10}, {19,14},
             {15, 4}, {15, 8}, {15,12}, {15,16},
        {11, 2}, {11, 6}, {11, 10}, {11,14}, {11,18},
             { 7, 4}, { 7, 8}, { 7,12}, { 7,16},
                  { 3, 6}, { 3,10}, { 3,14}
    };
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        // Set the location, discipline, and dice value of each region
        regions[regionID].location = locations[regionID];
        regions[regionID].discipline = discipline[regionID];
        regions[regionID].diceValue = dice[regionID];
        
        // Update the board struct with a reference to each region
        Cell* boardCell;
        boardCell = &board[locations[regionID].x][locations[regionID].y];
        boardCell->objectType = OBJECT_REGION;
        boardCell->region = &regions[regionID];
    }
}


// Given a board and regions (already initialised), this function will
// Initialise the vertices.
void initVertices(Board board, Region regions[], Vertex vertices[]) {
    int vertexNo = 0;
    // For each region, initialise a vertex on each corner of the hexagon
    // with the region as its centre (only once for each vertex)
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        for (int dir = 0; dir < DEGREES_IN_CIRCLE; dir += 60) {
            // Find the location of each vertex from the region
            Vector location = sum(regions[regionID].location,
                                  vectorBetweenRegionAndVertex(dir));
            Cell* boardCell = &board[location.x][location.y];
            
            if (boardCell->objectType == OBJECT_NONE) {
                // Record the vertex in the board struct
                boardCell->objectType = OBJECT_VERTEX;
                boardCell->vertex = &vertices[vertexNo];
                
                // Initialise the values of the vertex struct
                vertices[vertexNo].location = location;
                vertices[vertexNo].campus = VACANT_VERTEX;
                vertices[vertexNo].retrainingCentre = RETRAINING_NONE;
                vertexNo++;
            } 
        }
    }
}


// Given a board and regions (already initialised), this function will
// Initialise the arcs.
void initEdges(Board board, Region regions[], Edge edges[]) {
    int arcNo = 0;
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        for (int dir = 30; dir < DEGREES_IN_CIRCLE; dir += 60) {
            // Find the location of each arc near the region
            Vector location = sum(regions[regionID].location,
                                  vectorBetweenEdgeAndRegion(dir));
            assert(isOnBoard(location));
            // For the two invalid directions 90 and 270,
            // location will simply be the location of the given region,
            // so the check below will fail and nothing will be done
            Cell* boardCell = &board[location.x][location.y];
            
            if (boardCell->objectType == OBJECT_NONE) {
                
                // Record a reference to the arc in the board struct
                boardCell->objectType = OBJECT_EDGE;
                boardCell->edge = &edges[arcNo];
                
                // Set the initial values of the arc
                edges[arcNo].location = location;
                edges[arcNo].arc = VACANT_ARC;
                arcNo++;
            }
        }
    }
}


// Given an array of players, set their values to initial values
void initPlayers(Player players[]) {
    for (int player = UNI_A; player <= UNI_C; player++) {
        // Each player starts with one campus and one arc grant
        players[player].arcs = 2;
        players[player].normalCampuses = 2;
        players[player].GO8s = 0;
        players[player].publications = 0;
        players[player].patents = 0;
        for (int type = 0; type < NUM_STUDENT_TYPES; type++) {
            players[player].students[type] = 0;
        }
    }
}


// Given a board, set all its cells to contain nothing
void initBoard(Board board) {
    // Initialise grid struct to contain empty cells
    for (int column = 0; column < BOARD_COLUMNS; column++) {
        for (int row = 0; row < BOARD_ROWS; row++) {
            board[column][row].objectType = OBJECT_NONE;
            board[column][row].region = NULL;
            board[column][row].vertex = NULL;
            board[column][row].edge = NULL;
        }
    }
}


// Given a game, this function destroys it and frees the memory it used
void disposeGame(Game g) {
    printf("Game.c: disposing Game.\n");
    free (g);
}

// Given a game, this function returns the turn number (turns elapsed).
// The turn number is -1 before the game starts.
int getTurnNumber (Game g) {
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getTurnNumber called. returning %d\n", g->turnNumber);
#endif
    return g->turnNumber;
}


// make a new game, given the disciplines produced by each
// region, and the value on the dice discs in each region.
// note: each array must be NUM_REGIONS long
Game newGame (int discipline[], int dice[]) {
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: newGame Called\n");
    //printDisciplines(discipline);
    //printDice(dice);
#endif
    
    Game game = malloc(sizeof(GameStruct));
    
    // Initialise regions, arcs, vertices, and players
    initBoard(game->board);
    initRegions(game->board, game->regions, discipline, dice);
    initEdges(game->board, game->regions, game->edges);
    initVertices(game->board, game->regions, game->vertices);
    initPlayers(game->players);

    
    // Put in the initial campuses:

    // Player A:
    Vertex* vertex = vertexFromPath(INITIAL_PATH_A1, game->board);
    Edge* edge = edgeFromPath(INITIAL_PATH_A1, game->board);
    vertex->campus = CAMPUS_A;
    edge->arc = ARC_A;
    vertex = vertexFromPath(INITIAL_PATH_A2, game->board);
    edge = edgeFromPath(INITIAL_PATH_A2, game->board);
    vertex->campus = CAMPUS_A;
    edge->arc = ARC_A;

    // Player B:
    vertex = vertexFromPath(INITIAL_PATH_B1, game->board);
    edge = edgeFromPath(INITIAL_PATH_B1, game->board);
    vertex->campus = CAMPUS_B;
    edge->arc = ARC_B;
    vertex = vertexFromPath(INITIAL_PATH_B2, game->board);
    edge = edgeFromPath(INITIAL_PATH_B2, game->board);
    vertex->campus = CAMPUS_B;
    edge->arc = ARC_B;

    // Player C:
    vertex = vertexFromPath(INITIAL_PATH_C1, game->board);
    edge = edgeFromPath(INITIAL_PATH_C1, game->board);
    vertex->campus = CAMPUS_C;
    edge->arc = ARC_C;
    vertex = vertexFromPath(INITIAL_PATH_C2, game->board);
    edge = edgeFromPath(INITIAL_PATH_C2, game->board);
    vertex->campus = CAMPUS_C;
    edge->arc = ARC_C;

    
    // Initialise global game variables
    game->activePlayer = NO_ONE;
    game->turnNumber = -1;
    game->totalGO8s = 0;
    game->mostArcs = NO_ONE;
    game->mostPublications = NO_ONE;

    return game;
}

// Given a game struct, this function updates the values for which uni
// has the most arc grants, based on the values for each uni
void updateArcPrestigePoints(Game game) {
    int arcsA = game->players[UNI_A].arcs;
    int arcsB = game->players[UNI_B].arcs;
    int arcsC = game->players[UNI_C].arcs;
    
    if (arcsA > arcsB && arcsA > arcsC) {
        game->mostArcs = UNI_A;
    } else if (arcsB > arcsA && arcsB > arcsC) {
        game->mostArcs = UNI_B;
    } else if (arcsC > arcsB && arcsC > arcsA) {
        game->mostArcs = UNI_C;
    }
    // If none of them is more than the other two, nothing should be done
    // This ensures that prestige points are maintained until another
    // uni builds more arcs than the one who has the points
}

// Given a game struct, this function updates the values for which uni
// has the most publications, based on the values for each uni
void updatePublicationPrestigePoints(Game game) {
    int pubsA = game->players[UNI_A].publications;
    int pubsB = game->players[UNI_B].publications;
    int pubsC = game->players[UNI_C].publications;
    
    if (pubsA > pubsB && pubsA > pubsC) {
        game->mostPublications = UNI_A;
    } else if (pubsB > pubsA && pubsB > pubsC) {
        game->mostPublications = UNI_B;
    } else if (pubsC > pubsB && pubsC > pubsA) {
        game->mostPublications = UNI_C;
    }
    // If none of them is more than the other two, nothing should be done
    // This ensures that prestige points are maintained until another
    // uni gets more publications than the one who has the points
}


// make the specified action for the current player and update the
// game state accordingly.
void makeAction (Game g, action a) {
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: makeAction Called. Action: %s. ", actionString(a.actionCode));
    if (a.actionCode == BUILD_CAMPUS || a.actionCode == BUILD_GO8 ||
        a.actionCode == OBTAIN_ARC) {
        printf("Destination: \"%s\".\n", a.destination);
    }
    if (a.actionCode = RETRAIN_STUDENTS) {
        printf("From %s ", disciplineString(a.disciplineFrom));
        printf("to %s\n", disciplineString(a.disciplineTo));
    }
#endif
    int activePlayer = getWhoseTurn(g);
    if (a.actionCode == PASS){
        return;
    } else if (a.actionCode == BUILD_CAMPUS) {
        // Update the player's campus count:
        g->players[activePlayer].normalCampuses++;
        
        // Update the board to contain the campus:
        Vertex* vertex = vertexFromPath(a.destination, g->board);
        if (vertex == NULL) {
            printf("Error: Game.c: null vertex\n");
            return;
        }
        vertex->campus = campusForUni(activePlayer);

        // Deduct the students used to build the campus
        g->players[activePlayer].students[STUDENT_BPS]--;
        g->players[activePlayer].students[STUDENT_BQN]--;
        g->players[activePlayer].students[STUDENT_MJ]--;
        g->players[activePlayer].students[STUDENT_MTV]--;
        
    } else if (a.actionCode == BUILD_GO8) {
        // Update the player's GO8 count
        g->players[activePlayer].GO8s++;

        // Remove the normal campus to make room for the GO8 campus
        g->players[activePlayer].normalCampuses--;
        
        // Update the global GO8 count:
        g->totalGO8s++;
        
        // update the board to contain the GO8 campus
        Vertex* vertex = vertexFromPath(a.destination, g->board);
        if (vertex == NULL) {
            printf("error: Game.c: null vertex!\n");
            return;
        }
        vertex->campus = GO8ForUni(activePlayer);

        // Deduct the students used to build the GO8 campus
        g->players[activePlayer].students[STUDENT_MJ] -= 2;
        g->players[activePlayer].students[STUDENT_MMONEY] -= 3;
        
    } else if (a.actionCode == OBTAIN_ARC) {
        // Update the player's arc count
        g->players[activePlayer].arcs++;
        
        //update the board to contain the arc grant
        Edge* edge = edgeFromPath(a.destination, g->board);
        if (edge == NULL) {
            printf("Error: Game.c: null edge\n");
            return;
        }
        edge->arc = arcForUni(activePlayer);

        // Deduct the students used to build the Edge arc
        g->players[activePlayer].students[STUDENT_BQN]--;
        g->players[activePlayer].students[STUDENT_BPS]--;
        
        // Update the prestige points for haivng the most arc grants
        updateArcPrestigePoints(g);
        
    } else if (a.actionCode == OBTAIN_PUBLICATION) {
        // Deduct the students used to start the spinoff
        g->players[activePlayer].students[STUDENT_MJ]--;
        g->players[activePlayer].students[STUDENT_MTV]--;
        g->players[activePlayer].students[STUDENT_MMONEY]--;
        
        // Update the publication count for the player:
        g->players[activePlayer].publications++;
        
        // update publications prestige points
        updatePublicationPrestigePoints(g);
        
    } else if (a.actionCode == OBTAIN_IP_PATENT) {
        // Deduct the students used to start the spinoff
        g->players[activePlayer].students[STUDENT_MJ]--;
        g->players[activePlayer].students[STUDENT_MTV]--;
        g->players[activePlayer].students[STUDENT_MMONEY]--;
        
        // Update the patent count for the player:
        g->players[activePlayer].patents++;

    } else if (a.actionCode == RETRAIN_STUDENTS) {
        // Get student exchange rate between the requested disciplines
        int exchangeRate = getExchangeRate(g,
                                           activePlayer,
                                           a.disciplineFrom,
                                           a.disciplineTo);

        // Update the student counts
        g->players[activePlayer].students[a.disciplineFrom] -= exchangeRate;
        g->players[activePlayer].students[a.disciplineTo]++;
    }
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: makeAction: student counts after action:\n");
    printStudentCounts(g->players);
#endif    
}


// advance the game to the next turn,
// assuming that the dice has just been rolled and produced diceScore
void throwDice (Game g, int diceScore) {
    // Increment the turn number
    g->turnNumber++;

    // Use the modulus of the turn number to deduce whose turn it is
    if (g->turnNumber % 3 == 0) {
        g->activePlayer = UNI_A;
    } else if (g->turnNumber % 3 == 1) {
        g->activePlayer = UNI_B;
    } else if (g->turnNumber % 3 == 2) {
        g->activePlayer = UNI_C;
    }
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: throwDice Called with dice: %d. ", diceScore);
    printf("turnNumber: %d. ", g->turnNumber);
    printf("It is %s's turn.\n", uniString(g->activePlayer));
#endif

    // For each region with the same diceScore that was just rolled, all
    // players must be awarded 1 student of the type that region produces
    // for each campus they have in the region (2 for a GO8 campus)
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        if (g->regions[regionID].diceValue == diceScore) {
            
            // find which student type that region produces
            int discipline = g->regions[regionID].discipline;
            
            // Give students to each uni with a campus on the region
            Vector regionLocation = g->regions[regionID].location;
            // Check all the neighbouring vertices
            for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
                Vertex* vertex;
                vertex = relativeVertex(g->board, regionLocation, dir);
                if (vertex != NULL) {
                    // Find the uni which owns the campus
                    int owner = campusOwner(vertex->campus);
                    if (campusType(vertex->campus) == NORMAL_CAMPUS) {
                        g->players[owner].students[discipline]++;
                    } else if (campusType(vertex->campus) == GO8_CAMPUS) {
                        g->players[owner].students[discipline] += 2;
                    }
                }
            }
        }
    }

    // If a seven is rolled, all MTV and MMONEY students change to THDs
    // immediately after the students are produced
    if (diceScore == 7) {
        for (int uni = UNI_A; uni <= UNI_C; uni++) {
            g->players[uni].students[STUDENT_THD] += g->players[uni].students[STUDENT_MTV];
            g->players[uni].students[STUDENT_THD] += g->players[uni].students[STUDENT_MMONEY];
            g->players[uni].students[STUDENT_MTV] = 0;
            g->players[uni].students[STUDENT_MMONEY] = 0;
        }
    }
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: throwDice: student counts after changes:\n");
    printStudentCounts(g->players);
#endif
}


// what type of students are produced by the specified region?
// see discipline codes above
int getDiscipline (Game g, int regionID) {
    int result = g->regions[regionID].discipline;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getDiscipline called for region %d. ", regionID);
    printf("Returning %s\n", disciplineString(result));
#endif
    return result;
}


// what dice value produces students in the specified region?
// 2..12
int getDiceValue (Game g, int regionID) {
    int result = g->regions[regionID].diceValue;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getDiceValue called for region %d. ", regionID);
    printf("Returning %d\n", result);
#endif
    return result;
}


// which university currently has the prestige award for the most ARCs?
// this is NO_ONE until the first arc is purchased after the game
// has started.
int getMostARCs (Game g) {
    int result = g->mostArcs;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getMostArcs called. ");
    printf("Returning %s\n", uniString(result));
#endif
    return result;
}


// which university currently has the prestige award for the most pubs?
// this is NO_ONE until the first IP is patented after the game
// has started.
int getMostPublications (Game g) {
    int result = g->mostPublications;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getMostPublications called. ");
    printf("Returning %s\n", uniString(result));
#endif
    return result;
}


// return the player id of the player whose turn it is
// the result of this function is NO_ONE during Terra Nullis
int getWhoseTurn (Game g) {
    int result = g->activePlayer;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getWhoseTurn called. ");
    printf("Returning %s\n", uniString(result));
#endif
    return result;
}


// return the contents of the given vertex (ie campus code or
// VACANT_VERTEX)
int getCampus(Game g, path pathToVertex) {
    Vertex* vertex = vertexFromPath(pathToVertex, g->board);
    if (vertex == NULL) {
        printf("Error: Game.c: getCampus: null vertex\n");
        return VACANT_VERTEX;
    }
    int result = vertex->campus;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getCampus called for path \"%s\". ", pathToVertex);
    printf("Returning %s\n", campusString(result));
#endif
    return result;
}


// the contents of the given edge (ie ARC code or vacent ARC)
int getARC(Game g, path pathToEdge) {
    Edge* edge = edgeFromPath(pathToEdge, g->board);
    if (edge == NULL) {
        printf("Error: Game.c: null edge\n");
        return VACANT_ARC;
    }
    int result = edge->arc;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getARC called for path \"%s\". ", pathToEdge);
    printf("Returning %s\n", arcString(result));
#endif
    return result;
}


// returns TRUE if it is legal for the current
// player to make the specified move, FALSE otherwise.
// legal means everything is legal eg when placing a campus consider
// such things as:
//   * is the specified path a valid path?
//   * does it lead to a vacent vertex?
//   * under the rules of the game are they allowed to place a
//     campus at that vertex?  (eg is it adjacent to one of their ARCs?)
//   * does the player have the 4 specific students required to pay for
//     that campus?
// It is not legal to make any action during Terra Nullis ie
// before the game has started.
// It is not legal for a player to make the moves OBTAIN_PUBLICATION
// or OBTAIN_IP_PATENT (they can make the move START_SPINOFF)
int isLegalAction (Game g, action a) {
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: isLegalAction Called. Action: %s. ", actionString(a.actionCode));
    if (a.actionCode == BUILD_CAMPUS || a.actionCode == BUILD_GO8 ||
        a.actionCode == OBTAIN_ARC) {
        printf("Destination: \"%s\".\n", a.destination);
    }
    if (a.actionCode = RETRAIN_STUDENTS) {
        printf("From %s ", disciplineString(a.disciplineFrom));
        printf("to %s\n", disciplineString(a.disciplineTo));
    }
#endif
    int player = getWhoseTurn(g);
    if (player < 1) {
        // All actions are illegal if it is nobody's turn
        return FALSE;
    }

    if (a.actionCode == PASS) {
        // It is always legal to pass
        return TRUE;
    
    } else if (a.actionCode == BUILD_CAMPUS) {
        Vertex* baseVertex = vertexFromPath(a.destination, g->board);
        if (baseVertex == NULL) {
            printf("Error: Game.c: null vertex\n");
            return FALSE;
        }
        
        // Check if there are any campuses on the adjacent vertices
        int hasRoom = FALSE;
        // There is room unless there are campuses in adjacent vertices
        hasRoom = TRUE;
        for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
            Vertex* adjacentVertex;
            adjacentVertex = relativeVertex(g->board,
                                            baseVertex->location,
                                            dir);
            if (adjacentVertex != NULL) {
                if (adjacentVertex->campus != VACANT_VERTEX) {
                    // There is an adjacent vertex with a campus on it
                    hasRoom = FALSE;
                }
            }
        }
        
        // Check if the player has a arc on any of the adjacent edges
        int hasArc = FALSE;
        for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
            Edge* edge = relativeEdge(g->board, baseVertex->location, dir);
            if (edge != NULL) {
                if (arcOwner(edge->arc) == player) {
                    hasArc = TRUE;
                }
            }
        }
        
        // Get how many of the relevant students the player has
        int BPSs = g->players[player].students[STUDENT_BPS];
        int BQNs = g->players[player].students[STUDENT_BQN];
        int MJs = g->players[player].students[STUDENT_MJ];
        int MTVs = g->players[player].students[STUDENT_MTV];
        
        // Check if the player has enough of each to build a campus
        int hasStudents = FALSE;
        if (BPSs >= 1 && BQNs >= 1 && MJs >= 1 && MTVs >= 1) {
            hasStudents = TRUE;
        }
        
        // Building a campus is legal if all three conditions are met
        if (hasRoom == TRUE && hasArc == TRUE && hasStudents == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    
    } else if (a.actionCode == BUILD_GO8) {
        // Check if the player has the prerequisite campus at the vertex
        Vertex* vertex = vertexFromPath(a.destination, g->board);
        if (vertex == NULL) {
            printf("Error: Game.c: null vertex\n");
            return FALSE;
        }
        int hasCampus = FALSE;
        if (campusOwner(vertex->campus) == player &&
            campusType(vertex->campus) == NORMAL_CAMPUS) {
            hasCampus = TRUE;
        }

        // Get how many of the relevant students the player has
        int MJs = g->players[player].students[STUDENT_MJ];
        int MMs = g->players[player].students[STUDENT_MMONEY];

        // Check if the player has enough students to build a GO8
        int hasEnoughStudents = FALSE;
        if (MJs >= 2 && MMs >= 3) {
            hasEnoughStudents = TRUE;
        }

        // If both requirements are met, the action is legal
        if (hasEnoughStudents == TRUE && hasCampus == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    
    } else if (a.actionCode == OBTAIN_ARC) {
        // Check if the edge is vacant
        Edge* edge = edgeFromPath(a.destination,g->board);
        if (edge == NULL) {
            printf("Error: Game.c: null edge\n");
            return FALSE;
        }
        int edgeEmpty = FALSE;
        if (edge->arc == VACANT_ARC) {
            edgeEmpty = TRUE;
        }

        // Check if the player already has an arc adjacent to this edge
        int HasPath = FALSE;
        for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
            Edge* adjacentEdge;
            adjacentEdge = relativeEdge(g->board,
                                        edge->location,
                                        dir);
            if (adjacentEdge != NULL) {
                if (arcOwner(adjacentEdge->arc) == player) {
                    // The player has a arc on an adjacent arc
                    HasPath = TRUE;
                }
            }
        }
        
        // Check whether the player has enough students for an arc grant
        int BQNs = g->players[player].students[STUDENT_BQN];
        int BPSs = g->players[player].students[STUDENT_BPS];
        int hasStudents = FALSE;
        if (BQNs >= 1 && BPSs >= 1) {
            hasStudents = TRUE;
        }

        // The move is legal if all three requirements are met
        if (edgeEmpty == TRUE && hasStudents == TRUE && HasPath == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    } else if (a.actionCode == START_SPINOFF) {
        // Get the number of relevant students
        int MJs = g->players[player].students[STUDENT_MJ];
        int MTVs = g->players[player].students[STUDENT_MTV];
        int MMs = g->players[player].students[STUDENT_MMONEY];

        // Check if the player has enough students
        if (MJs >= 1 && MTVs >= 1 && MMs >= 1) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    } else if (a.actionCode == RETRAIN_STUDENTS) {

        // THDs Cannot be retrained
        if (a.disciplineFrom == STUDENT_THD) {
            return FALSE;
        }
        
        // Get the exchange rate between the requested students types
        int exchangeRate = getExchangeRate(g,
                                           player,
                                           a.disciplineFrom,
                                           a.disciplineTo);
        // Check how many of the source students the player has
        int studentsFrom = g->players[player].students[a.disciplineFrom];
        // If the player has enough students, the move is legal
        if (studentsFrom >= exchangeRate) {
            return TRUE;
        } else {
            return FALSE;
        }
    } else {
        printf("Error: Game.c: isLegalAction received invalid");
        printf(" actionCode %d\n", a.actionCode);
        return FALSE; // The action code was invalid
    }
}


// return the number of KPI points the specified player currently has
int getKPIpoints (Game g, int player) {
    int kpi = 0;
    kpi += KPI_PER_ARC * g->players[player].arcs;
    kpi += KPI_PER_CAMPUS * g->players[player].normalCampuses;
    kpi += KPI_PER_GO8 * g->players[player].GO8s;
    kpi += KPI_PER_PATENT * g->players[player].patents;

    if (g->mostArcs == player) {
        kpi += POINTS_FOR_MOST_ARCS;
    }

    if (g->mostPublications == player) {
        kpi += POINTS_FOR_MOST_PUBLICATIONS;
    }
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getKPIpoints Called for %s.", uniString(player));
    printf(" Returning %d\n", kpi);
#endif
    return kpi;
}


// return the number of arc grants the specified player currently has
int getARCs (Game g, int player) {
    int result = g->players[player].arcs;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getArcs called for player %s. ", uniString(player));
    printf("Returning %d\n", result);
#endif
    return result;
}


// return the number of GO8 campuses the specified player currently has
int getGO8s (Game g, int player) {
    int result = g->players[player].GO8s;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getGO8s called for player %s. ", uniString(player));
    printf("Returning %d\n", result);
#endif
    return result;
}


// return the number of normal Campuses the specified player currently has
int getCampuses (Game g, int player) {
    int result = g->players[player].normalCampuses + g->players[player].GO8s;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getCampuses called for player %s", uniString(player));
    printf(". Returning %d\n", result);
#endif
    return result;
}


// return the number of IP Patents the specified player currently has
int getIPs (Game g, int player) {
    int result = g->players[player].patents;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getIPs called for player %s. ", uniString(player));
    printf("Returning %d\n", result);
#endif
    return result;
}


// return the number of Publications the specified player currently has
int getPublications (Game g, int player) {
    int result = g->players[player].publications;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getPublications called for player %s", uniString(player));
    printf(". Returning %d\n", result);
#endif
    return result;
}


// return the number of students of the specified discipline type
// the specified player currently has
int getStudents (Game g, int player, int discipline) {
    int result = g->players[player].students[discipline];
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getStudents called for player %s", uniString(player));
    printf(" in discipline %s. ", disciplineString(result));
    printf("Returning %d\n", result);
#endif
    return result;
}


// return how many students of discipline type disciplineFrom
// the specified player would need to retrain in order to get one
// student of discipline type disciplineTo.  This will depend
// on what retraining centers, if any, they have a campus at.
int getExchangeRate (Game g,
                     int player,
                     int disciplineFrom,
                     int disciplineTo) { // TODO
    int result = 3;
#ifdef PRINT_INTERFACE_CALLS
    printf("Game.c: getExchangeRate called for player %s", uniString(player));
    printf(". Returning %d\n", result);
#endif
    return result;
}
